package kh.dynamodb.lowlevel;

import java.text.ParseException;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

import kh.dynamodb.model.DomainExample1;

/**
 * Example use of DynamoDB low level api.
 * 
 * @author kevinhooke
 */
public class DynamoDBLowlevelExample {

	private AmazonDynamoDB client;
	
	public DynamoDBLowlevelExample() {
		
		this.client = AmazonDynamoDBClientBuilder.standard()
			  .withRegion(Regions.US_WEST_1)
			  .build();
	}
	
	/**
	 * Save DomainExample1 using low level apis.
	 * 
	 * @param example
	 */
	public void saveExample1(DomainExample1 example) {
		DynamoDB dynamodb = new DynamoDB(this.client);
		Table table = dynamodb.getTable("dynamodb-test");

        Item item = new Item()
        	.withPrimaryKey("testid", example.getTestid())
        	.withString("teststring1", example.getTestString1())
            .withNumber("testInt1", example.getTestInt1())
            .withBoolean("testBoolean", example.isTestBoolean())
            .withString("testDate", example.getDate().toString());
        	
        table.putItem(item);
	}
	
	/**
	 * Retrieves DomainExample1 using low level getItem() api.
	 * 
	 * @param key
	 * @return the retrieved DomainExample1
	 * @throws ParseException
	 */
	public DomainExample1 getExample1(String key) {
		DynamoDB dynamodb = new DynamoDB(this.client);
		Table table = dynamodb.getTable("dynamodb-test");

        Item item = table.getItem("testid", key, "testid, teststring1, testInt1, testBoolean, testDate", null);
        DomainExample1 result = new DomainExample1();
        result.setTestid(item.getString("testid"));
        result.setTestString1(item.getString("teststring1"));
        result.setTestInt1(item.getInt("testInt1"));
        result.setTestBoolean(item.getBoolean("testBoolean"));
        String dateString = item.getString("testDate");
        //TODO: parse date string and set to result here
        //result.setDate(parsedDate);
        return result;
	}
	
}
