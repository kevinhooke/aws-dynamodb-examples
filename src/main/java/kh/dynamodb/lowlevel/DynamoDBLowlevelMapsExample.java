package kh.dynamodb.lowlevel;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

import kh.dynamodb.model.DomainExample1;

public class DynamoDBLowlevelMapsExample {

	private AmazonDynamoDB client;
	
	public DynamoDBLowlevelMapsExample() {
		
		this.client = AmazonDynamoDBClientBuilder.standard()
			  .withRegion(Regions.US_WEST_1)
			  .build();
	}
	
	public void insertItemWithMap(DomainExample1 example) {
		
		DynamoDB dynamodb = new DynamoDB(this.client);
		Table table = dynamodb.getTable("dynamodb-test");

        Item item = new Item()
        	.withPrimaryKey("testid", example.getTestid())
        	.withString("teststring1", example.getTestString1())
            .withNumber("testInt1", example.getTestInt1())
            .withBoolean("testBoolean", example.isTestBoolean())
            .withString("testDate", example.getDate().toString())
            .withMap("exampleMap", example.getExampleMap());
        	
        table.putItem(item);
	}
	
	public DomainExample1 getExample1(String key) {
		DynamoDB dynamodb = new DynamoDB(this.client);
		Table table = dynamodb.getTable("dynamodb-test");

        Item item = table.getItem("testid", key, "testid, teststring1, testInt1, testBoolean, testDate, exampleMap", null);
        DomainExample1 result = new DomainExample1();
        result.setTestid(item.getString("testid"));
        result.setTestString1(item.getString("teststring1"));
        result.setTestInt1(item.getInt("testInt1"));
        result.setTestBoolean(item.getBoolean("testBoolean"));
        String dateString = item.getString("testDate");
        //TODO: parse date string and set to result here
        //result.setDate(parsedDate);
        
        result.setExampleMap(item.getMap("exampleMap"));
        return result;
	}
	
}
