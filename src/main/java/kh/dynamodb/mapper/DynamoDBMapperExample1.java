package kh.dynamodb.mapper;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import kh.dynamodb.model.DomainExample1;


public class DynamoDBMapperExample1 {

	private AmazonDynamoDB client;
	
	public DynamoDBMapperExample1() {
		
		this.client = AmazonDynamoDBClientBuilder.standard()
			  .withRegion(Regions.US_WEST_1)
			  .build();
	}
	
	/**
	 * Saves DomainExample1 using DynamoDBMapper.
	 * 
	 * @param example1
	 */
	public void saveDomainExample1(DomainExample1 example1) {
		DynamoDBMapper mapper = new DynamoDBMapper(this.client);
		mapper.save(example1);
	}

	/**
	 * Retrieves DomainExample1 using DynamoDBMapper.
	 * 
	 * @param example1 the key to retrieve
	 */
	public DomainExample1 getDomainExample1(String key) {
		DynamoDBMapper mapper = new DynamoDBMapper(this.client);
		DomainExample1 retrieved = mapper.load(DomainExample1.class, key);
		return retrieved;
	}

}
