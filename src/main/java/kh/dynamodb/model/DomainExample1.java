package kh.dynamodb.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * POJO used with the low-level api examples and the high-level DynamoDBMapper examples.
 * 
 * The @DynamoDB annotations are only used with used with the DynamoDBMapper.
 * 
 * Supported Java SDK type examples: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBMapper.DataTypes.html
 */
@DynamoDBTable(tableName = "dynamodb-test")
public class DomainExample1 {

	@DynamoDBHashKey
	private String testid;
	
	private String testString1;
	private boolean testBoolean; //maps to DynamoDB number 0 or 1
	private Date date = new Date(); //maps to DynamoDB String
	private int testInt1;
	
	private Map<String, String> exampleMap = new HashMap<>();
	
	public String getTestString1() {
		return testString1;
	}
	public void setTestString1(String testString1) {
		this.testString1 = testString1;
	}
	public boolean isTestBoolean() {
		return testBoolean;
	}
	public void setTestBoolean(boolean testBoolean) {
		this.testBoolean = testBoolean;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getTestInt1() {
		return testInt1;
	}
	public void setTestInt1(int testInt1) {
		this.testInt1 = testInt1;
	}
	public String getTestid() {
		return testid;
	}
	public void setTestid(String testid) {
		this.testid = testid;
	}
	public Map<String, String> getExampleMap() {
		return exampleMap;
	}
	public void setExampleMap(Map<String, String> exampleMap) {
		this.exampleMap = exampleMap;
	}
	
}
