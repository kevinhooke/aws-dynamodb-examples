package kh.dynamodb.lowlevel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import kh.dynamodb.model.DomainExample1;

public class DynamoDBLowLevelExampleTest {

	@Test
	public void testSaveAndGetItemLowLevel() {
		DynamoDBLowlevelExample exampleLowLevel = new DynamoDBLowlevelExample();
		
		DomainExample1 domain1 = new DomainExample1();
		domain1.setTestid("testlow1");
		domain1.setTestString1("test");
		domain1.setTestBoolean(true);
		domain1.setTestInt1(100);
		Date now = new Date();
		domain1.setDate(now);
		
		exampleLowLevel.saveExample1(domain1);
		
		DomainExample1 retrieved = exampleLowLevel.getExample1("testlow1");
		assertNotNull(retrieved);
		assertEquals("testlow1", retrieved.getTestid());
		assertEquals("test", retrieved.getTestString1());
		assertTrue(retrieved.isTestBoolean());
		assertEquals(100, retrieved.getTestInt1());
		//TODO: assert after date string parsed and set
		//assertEquals(now, retrieved.getDate());
	}
	
}
