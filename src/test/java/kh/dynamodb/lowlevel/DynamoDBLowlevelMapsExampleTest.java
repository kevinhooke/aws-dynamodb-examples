package kh.dynamodb.lowlevel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import kh.dynamodb.model.DomainExample1;

public class DynamoDBLowlevelMapsExampleTest {

	@Test
	public void testSaveAndGetItemLowLevelWithMap() {
		DynamoDBLowlevelMapsExample exampleLowLevelMaps = new DynamoDBLowlevelMapsExample();
		
		DomainExample1 domain1 = new DomainExample1();
		domain1.setTestid("testlow2withmap");		
		domain1.setTestString1("test");
		domain1.setTestBoolean(true);
		domain1.setTestInt1(100);
		Date now = new Date();
		domain1.setDate(now);
		
		Map<String, String> map = new HashMap<>();
		map.put("item1","value1");
		map.put("item2","value2");
		map.put("item3","value3");
		domain1.setExampleMap(map);
		
		exampleLowLevelMaps.insertItemWithMap(domain1);
		
		DomainExample1 retrieved = exampleLowLevelMaps.getExample1("testlow2withmap");
		assertNotNull(retrieved);
		assertEquals("value1", retrieved.getExampleMap().get("item1"));
		assertEquals("value2", retrieved.getExampleMap().get("item2"));
		assertEquals("value3", retrieved.getExampleMap().get("item3"));
	}
	
}
