package kh.dynamodb.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import kh.dynamodb.mapper.DynamoDBMapperExample1;
import kh.dynamodb.model.DomainExample1;

public class DynamoDBMapperExample1Test {

	@Test
	public void testTestDomainMapper1() {
		DynamoDBMapperExample1 exampleDynamo = new DynamoDBMapperExample1();
		
		DomainExample1 domain1 = new DomainExample1();
		domain1.setTestid("test1mapper");
		domain1.setTestString1("test");
		domain1.setTestBoolean(true);
		domain1.setTestInt1(100);
		Date now = new Date();
		domain1.setDate(now);
		Map<String, String> map = new HashMap<>();
		map.put("item1","value1");
		map.put("item2","value2");
		map.put("item3","value3");
		domain1.setExampleMap(map);
		
		exampleDynamo.saveDomainExample1(domain1);
		
		DomainExample1 retrieved = exampleDynamo.getDomainExample1("test1mapper");
		assertNotNull(retrieved);
		assertEquals("test1mapper", retrieved.getTestid());
		assertEquals("test", retrieved.getTestString1());
		assertTrue(retrieved.isTestBoolean());
		assertEquals(100, retrieved.getTestInt1());
		assertEquals(now, retrieved.getDate());
		
		assertEquals("value1", retrieved.getExampleMap().get("item1"));
		assertEquals("value2", retrieved.getExampleMap().get("item2"));
		assertEquals("value3", retrieved.getExampleMap().get("item3"));
	}
}
